$(document).ready(function () {
    $('.js-header__search').click(function () {
        console.log('clicked button search');
    });
    $('.js-header__language').click(function () {
        console.log('clicked button PT');
    });
    $('.js-header__login-button').click(function () {
        console.log('clicked button login');
    });
    $('.js-header__toggle-item1').click(function () {
        console.log('clicked button Plants and flowers');
    });
    $('.js-header__toggle-item2').click(function () {
        console.log('clicked button Plant care');
    });
    $('.js-header__toggle-item3').click(function () {
        console.log('clicked button News');
    });

    const menuBtn = $('.js-header__menu');
    const menu = $('.js-header__toggle');

    menuBtn.on('click', function () {
        if ($(this).hasClass('is-active')) {
            $(this).removeClass('is-active');
            menu.slideUp(300);
        } else {
            $(this).addClass('is-active');
            menu.slideDown(300);
        }
    });

    $(document).click(function (e) {
        if (
            !menuBtn.is(e.target) &&
			!menu.is(e.target) &&
			menu.has(e.target).length === 0
        ) {
            menu.slideUp(300);
            menuBtn.removeClass('is-active');
        }
    });
});
