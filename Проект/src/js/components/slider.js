$(document).ready(function () {
    $('.js-slider__items').slick({
        arrows: false,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
    });
});
