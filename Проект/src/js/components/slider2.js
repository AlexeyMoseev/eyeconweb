$(document).ready(function () {
    $('.js-slider2__items').slick({
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 2,
    });

    $('.js-slider2__item-button').click(function () {
        console.log('clicked button Go Over');
    });

    $('.js-slider2__heading-button').click(function () {
        console.log('clicked button See all');
    });
});
